pragma solidity >= 0.5.0;

import "./Registrar.sol";
import "./Resolvers.sol";

//      M - TA  constructor(string memory prop, address chair) public
//      TA      function proposal() external view returns (string memory)
//      TA      function status() external view returns (uint)
//      DEP     function register() public notRegistered(msg.sender)
//      DEP     function getRequests() public view onlyChairperson returns (address[] memory)
//      DEP     function giveRightToVote(address voter) public onlyChairperson notStarted registered(voter) notAuthorized(voter)
//      DEP     function isRegistered(address voter) external view returns (bool)
//      DEP     function canVote(address voter) external view returns (bool)
//      DEP     function numVoters() external view returns (uint)
//      DEP     function numPending() public view returns (uint)
//      DEP     function voterAddress(uint index) external view returns (address)
//      DEP     function start() public onlyChairperson notStarted
//      TA      function started() external view returns (bool)
//      DEP     function end() public onlyChairperson hasStarted notEnded
//      TA      function ended() external view returns (bool)
//      TA      function vote(bool option) public hasStarted notEnded{
//      M - TA  function hasVoted(address voter) external view returns (bool)
//      DEP     function hasRegistered(address voter) external view returns (bool)
//      DEP     function votedFor(address voter) external view returns (bool)
//      TA      function result() external view returns (uint yes, uint no)

// isRegisteredToVote(string memory voter)
// isAuthorizedToVote(string memory voter)

contract Ballot {

    Registrar private registrar;
    Resolvers private resolvers;

    string private proposalName;
    string private proposalDescription;
    uint private start;
    uint private end;
    mapping(bool => uint) private votes;
    mapping(address => bool) private voted;

    modifier hasStarted() {
        require(block.timestamp >= start, "La votacion no ha comenzado");
        _;
    }

    modifier hasEnded() {
        require(block.timestamp >= end, "La votacion no ha terminado");
        _;
    }

    modifier hasNotEnded() {
        require(block.timestamp < end, "La votacion ya ha terminado");
        _;
    }

    modifier isRegistered() {
        require(isRegisteredToVote(), "No tiene derecho a votar");
        _;
    }

    modifier isAuthorized() {
        require(isAuthorizedToVote(), "No tiene derecho a votar");
        _;
    }

    modifier hasNotVoted() {
        require(!voted[msg.sender], "El votante ya voto");
        _;
    }

    modifier assertDuration(uint startTime, uint duration) {
        require(startTime + duration > block.timestamp , "La propuesta debe durar mas tiempo");
        _;
    }

    constructor(string memory propName, string memory propDescription, uint startTime, uint duration, address registrarAddress) assertDuration(startTime, duration) public {
        proposalName = propName;
        proposalDescription = propDescription;
        start = startTime;
        end = startTime + duration;
        
        registrar = Registrar(registrarAddress);
        resolvers = Resolvers(registrar.getResolvers());
    }

    // Get proposal
    function proposal() external view returns (string memory) {
        return proposalDescription;
    }

    // Returns if msg sender is registered to vote
    function isRegisteredToVote() public view returns (bool) {
        bytes32 node = registrar.getResolver(msg.sender);
        return node != bytes32(0);
    }

    // Returns if msg sender is authorized to vote
    function isAuthorizedToVote() public view returns (bool) {
        if (!isRegisteredToVote()) {
            return false;
        } else {
            bytes32 node = registrar.getResolver(msg.sender);
            string memory textRecord = resolvers.text(node, proposalName);
            return keccak256(abi.encodePacked((textRecord))) != keccak256(abi.encodePacked(("")));
        }
    }

    // Returns if msg sender has already vote to vote
    function hasAlreadyVoted() external view returns (bool) {
        return voted[msg.sender];
    }

    // Return start time
    function startTime() public view returns (uint) {
        return start;
    }

    // Return end time
    function endTime() public view returns (uint) {
        return end;
    }

    // Return if the ballot start
    function started() public view returns (bool) {
        return block.timestamp >= start;
    }

    // Return if the ballot end
    function ended() public view returns (bool) {
        return block.timestamp >= end;
    }

    // Vote for the ballot, recieve conformity
    function vote(bool option) external hasStarted hasNotEnded isAuthorized hasNotVoted {
        voted[msg.sender] = true;
        votes[option]++;
    }

    // Return results and partial results if still running
    function results() external view returns (uint yes, uint no) {
        return (votes[true], votes[false]);
    }
}
