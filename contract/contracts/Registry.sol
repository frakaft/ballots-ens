pragma solidity >= 0.5.0;

contract Registry {

    address private registrar;

    mapping(string => bytes32) private resolvers;

    modifier onlyRegistrar() {
        require(msg.sender == registrar, "No permitido");
        _;
    }

    modifier available(string memory name) {
        require(resolvers[name] == bytes32(0), "Nombre no disponible");
        _;
    }

    modifier isRegistered(string memory name) {
        require(resolvers[name] != bytes32(0), "El no esta registrado");
        _;
    }

    constructor(string memory name, bytes32 resolver) public {
        registrar = msg.sender;
        register(name, resolver);
    }

    // Register name and resolver resolver
    function register(string memory name, bytes32 resolver) public onlyRegistrar() available(name) {
        resolvers[name] = resolver;
    }

    // Get resolver of name
    function getResolver(string memory name) public view returns (bytes32) {
        return resolvers[name];
    }
}