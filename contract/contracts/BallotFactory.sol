pragma solidity >= 0.5.0;

import "./Ballot.sol";
import "./Registrar.sol";
import "./Resolvers.sol";

contract BallotFactory {

    Registrar private registrar;
    Resolvers private resolvers;

    string constant private DOMAIN = "iua";
    string constant private CONTRACTS_SUBDOMAIN = "contratos";
    string constant private VOTERS_SUBDOMAIN = "votantes";

    string[] private ballots;

    uint private timestamp;

    mapping(string => string[]) private ballotsByOwner;

    modifier ballotExist(uint index) {
        require(index < ballots.length, "No existe la propuesta");
        _;
    }

    modifier isRegistered() {
        require(hasName(), "No se encuentra registrado");
        _;
    }

    modifier notBlank(string memory key) {
        require((keccak256(abi.encodePacked((key))) != keccak256(abi.encodePacked(("")))), "Sin definir");
        _;
    }

    constructor() public {
        registrar = new Registrar(DOMAIN);
        resolvers = Resolvers(registrar.getResolvers());
    }

    // Create ballot and register them to ENS
    function registerBallot(string calldata proposalName, string calldata proposalDescription, uint startTime, uint duration) external 
            isRegistered notBlank(proposalName) notBlank(proposalDescription) {
        Ballot ballot = new Ballot(string(abi.encodePacked(proposalName, '.', CONTRACTS_SUBDOMAIN, '.', DOMAIN)), proposalDescription, startTime, duration, address(registrar));
        
        string memory ballotName = registrar.register(string(abi.encodePacked(proposalName, '.', CONTRACTS_SUBDOMAIN)), address(ballot));
        
        bytes32 ownerNode = registrar.getResolver(msg.sender);
        string memory ownerName = resolvers.name(ownerNode);


        // Set the sender name on the record
        registrar.setText(ownerNode, ballotName, ownerName);

        ballots.push(ballotName);
        ballotsByOwner[ownerName].push(ballotName);
    }

    // Return number of created ballots
    function createdCount() external view returns (uint) {
        return ballots.length;
    }

    // Return name of ballot by index
    function created(uint index) external view ballotExist(index) returns (string memory) {
        return ballots[index];
    }

    // Return numer of created ballots by owner name
    function createdByCount(string memory owner) public view notBlank(owner) returns (uint) {
        return ballotsByOwner[owner].length;
    }

    // Return name of ballot by owner and index
    function createdBy(string memory owner, uint index) public view notBlank(owner) returns (string memory) {
        uint length = ballotsByOwner[owner].length;
        require(index < length, "No existe la propuesta");
        return ballotsByOwner[owner][index];
    }

    // Register voter to ENS
    function registerVoter(string calldata voterName) external notBlank(voterName) {
        registrar.register(string(abi.encodePacked(voterName, '.', VOTERS_SUBDOMAIN)), msg.sender);
    }

    // Returns address by name in ENS
    function resolveAddress(string calldata name) external view notBlank(name) returns (address) {
        bytes32 node = registrar.getResolver(name);
        return resolvers.addr(node);
    }

    // Reverce name resolution in ENS
    function resolveName() external view returns (string memory) {
        bytes32 node = registrar.getResolver(msg.sender);
        return resolvers.name(node);
    }

    // Check if msg sender is registered en the ENS
    function hasName() public view returns (bool) {
        bytes32 node = registrar.getResolver(msg.sender);
        return node != bytes32(0);
    }

    // Give right to vote to voter by ballot name and voter name
    function giveRightToVote(string calldata voterName, string calldata ballotName) external notBlank(voterName) notBlank(ballotName) isRegistered {
        bytes32 ballotNode = registrar.getResolver(ballotName);
        bytes32 voterNode = registrar.getResolver(voterName);
        require(ballotNode != bytes32(0) && voterNode != bytes32(0), "No se pudo realizar esta accion");
        
        bytes32 senderNode = registrar.getResolver(msg.sender);
        string memory textRecord = resolvers.text(senderNode, ballotName);

        require(keccak256(abi.encodePacked((textRecord))) != keccak256(abi.encodePacked((""))), "No tiene permisos para realizar esta accion");

        // Set the sender name on the record
        string memory senderName = resolvers.name(senderNode);
        registrar.setText(voterNode, ballotName, senderName);
    }

    // Remove right to vote to voter by ballot name and voter name
    function removeRightToVote(string calldata voterName, string calldata ballotName) external notBlank(voterName) notBlank(ballotName) isRegistered {
        bytes32 ballotNode = registrar.getResolver(ballotName);
        bytes32 voterNode = registrar.getResolver(voterName);
        require(ballotNode != bytes32(0) && voterNode != bytes32(0), "No se pudo realizar esta accion");
        
        bytes32 senderNode = registrar.getResolver(msg.sender);
        string memory textRecord = resolvers.text(senderNode, ballotName);

        require(keccak256(abi.encodePacked((textRecord))) != keccak256(abi.encodePacked((""))), "No tiene permisos para realizar esta accion");

        // Set to empty the record
        registrar.setText(voterNode, ballotName, "");
    }

    // Get text records of msg sender by key
    function getText(string calldata key) external view returns (string memory) {
        bytes32 senderNode = registrar.getResolver(msg.sender);
        return resolvers.text(senderNode, key);
    }

    // Update timestamp
    function stamptime() external {
        timestamp = block.timestamp;
    }
}