pragma solidity >= 0.5.0;

import "./Registry.sol";
import "./Resolvers.sol";

contract Registrar {

    address private factory;

    string private domain;

    Registry private registry;
    Registry private reverseRegistry;
    Resolvers private resolvers;

    modifier onlyFactory() {
        require(msg.sender == factory, "No permitido");
        _;
    }

    constructor(string memory name) public {
        factory = msg.sender;
        domain = name;
        address addr = address(this);
        resolvers = new Resolvers();
        bytes32 node = resolvers.resolve(addr, domain);
        registry = new Registry(name, node);
        reverseRegistry = new Registry(string(abi.encodePacked(addr)), node);
    }

    // Register name on ENS
    function register(string memory name, address owner) public onlyFactory returns (string memory) {
        string memory fullName = string(abi.encodePacked(name, '.', domain));
        bytes32 node = resolvers.resolve(owner, fullName);
        registry.register(fullName, node);
        reverseRegistry.register(string(abi.encodePacked(owner)), node);
        return fullName;
    }

    // Return resolvers address
    function getResolvers() public view returns (address) {
        return address(resolvers);
    }

    // Return resolver node
    function getResolver(string memory name) public view returns (bytes32) {
        return registry.getResolver(name);
    }

    // Return reverse resolver node
    function getResolver(address owner) public view returns (bytes32) {
        return reverseRegistry.getResolver(string(abi.encodePacked(owner)));
    }

    // Set text record on node
    function setText(bytes32 node, string memory key, string memory value) public onlyFactory {
        resolvers.setText(node, key, value);
    }

}