pragma solidity >= 0.5.0;

contract Resolvers {

    bytes4 constant private SUPPORTS_INTERFACE_ID = bytes4(keccak256("supportsInterface(bytes4)"));
    bytes4 constant private ADDR_ID = bytes4(keccak256("addr(bytes32)"));
    bytes4 constant private NAME_ID = bytes4(keccak256("name(bytes32)"));
    bytes4 constant private TEXT_ID = bytes4(keccak256("text(bytes32,string)"));
    bytes4 constant private SET_TEXT_ID = bytes4(keccak256("setText(bytes32,string,string)"));

    address private registrar;

    uint256 private nextNode;

    struct Node {
        address addr;
        string name;
    }

    mapping(bytes32 => Node) private nodes;

    mapping(bytes32 => mapping(string => string)) private text_records;

    modifier onlyRegistrar() {
        require(msg.sender == registrar, "No permitido");
        _;
    }

    modifier notNull(bytes32 node) {
        require(node != bytes32(0), "Sin definir");
        _;
    }

    modifier notBlank(string memory key) {
        require((keccak256(abi.encodePacked((key))) != keccak256(abi.encodePacked(("")))), "Sin definir");
        _;
    }

    constructor() public {
        registrar = msg.sender;
    }

    // Set a new node. Takes node key, address and name.
    function resolve(address nodeAddress, string memory nodeName) public onlyRegistrar returns (bytes32) {
        bytes32 node = bytes32(nextNode++);
        nodes[node].addr = nodeAddress;
        nodes[node].name = nodeName;
        return node;
    }

    // Takes an interface ID and returns a boolean value indicating if this interface is supported or not.
    function supportsInterface(bytes4 interfaceID) public pure returns (bool) {
        return interfaceID == SUPPORTS_INTERFACE_ID ||
                interfaceID == ADDR_ID ||
                interfaceID == NAME_ID ||
                interfaceID == TEXT_ID ||
                interfaceID == SET_TEXT_ID;
    }

    // Returns the Ethereum address associated with the provided node, or 0 if none.
    function addr(bytes32 node) public view notNull(node) returns (address) {
        require(node != bytes32(0), "null");
        return nodes[node].addr;
    }

    // Returns the canonical ENS name associated with the provided node. Used exclusively for reverse resolution.
    function name(bytes32 node) public view notNull(node) returns (string memory) {
        require(node != bytes32(0), "null");
        return nodes[node].name;
    }


    // Retrieves text metadata for node. Each name may have multiple pieces of metadata, identified by a unique string key.
    // If no text data exists for node with the key key, the empty string is returned.
    function text(bytes32 node, string memory key) public view notNull(node) returns (string memory) {
        return text_records[node][key];
    }

    // Sets text metadata for node with the unique key key to value, overwriting anything previously stored for node and key.
    // To clear a text field, set it to the empty string.
    function setText(bytes32 node, string memory key, string memory value) public notNull(node) notBlank(key) onlyRegistrar {
        require(nodes[node].addr != address(0), "El registro no esta configurado");
        text_records[node][key] = value;
    }

}