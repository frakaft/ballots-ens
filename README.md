# Trabajo PrÃ¡ctico Final

## Despliegue

* Ejecutar servidor de ganash que proporcione una red de ethereum en el puerto 7545 https://www.trufflesuite.com/ganache

* Posicionarce en el directorio contracts y desplegar los contratos con el cliente de truffle https://www.trufflesuite.com/truffle
```
$ cd contracts
$ truffle migrate
```

* Desplegar la aplicacion web
```
$ cd ui
$ npm install
$ npm run serve
```

* Para probar la aplicacion web, se debe instalar Metamask en su navegador https://metamask.io/download

## Enunciado

Este trabajo implica una modificaciÃ³n y extensiÃ³n del Trabajo PrÃ¡ctico 6, y debe ser presentado una semana antes de la fecha del examen final.
El examen final consistirÃ¡ en presentaciÃ³n del trabajo y eventual aplicaciÃ³n de modificaciones.

El proyecto debe incluir dos componentes principales:

* Un conjunto de _smart contracts_, ubicados en el subdirectorio `contracts`. Dicho subdirectorio debe tener la estructura de un proyecto `truffle`, y debe ser posible desplegar todos los contratos relevantes simplemente ejecutando `truffle deploy` (o `truffle migrate`). Desde la consola de `truffle` debe ser posible acceder a las instancias desplegadas mediante el mÃ©todo `deployed()` de un  _truffle contract_.
* Interface web, en el directorio `ui`. El directorio debe contener un archivo `README.md` que indica los pasos a seguir para desplegar el servicio.

## DescripciÃ³n general
Debe proveerse un sistema de votaciÃ³n similar al desarrollado en el Trabajo PrÃ¡ctico 6, con las modificaciones especificadas en este documento. A diferencia de los prÃ¡cticos anteriores, no se provee ningÃºn prototipo de los contratos ni de la interface web.

Provea la documentaciÃ³n correspondiente a los contratos utilizados. Para cada funciÃ³n de los contratos especifique su funcionalidad, argumentos, valores devueltos y condiciones de error, ya sea como comentarios en el cÃ³digo o en el `README.md` correspondiente.

En todos los casos en los que la especificaciÃ³n pueda interpretarse de manera ambigua, tome una decisiÃ³n y documÃ©ntela adecuadamente.

Si bien no es obligatorio, se considerarÃ¡ favorablemente la provisiÃ³n de _casos de prueba_.

## ENS
A nivel de interface, deben reemplazarse todas las direcciones, tanto de contratos como de usuarios, por nombres registrados en un ENS.

Por lo tanto, el conjunto de _smart contracts_ debe contener los contratos necesarios para implementar estas funcionalidades. Esto implica, como mÃ­nimo:

* Un registro (_registry_).
* Uno o mÃ¡s registradores (_registrars_).
* Uno o mÃ¡s resolutores (_resolvers_).

Estos contratos deben ajustarse a las especificaciones de la [documentaciÃ³n](https://docs.ens.domains/). En particular, para los [_resolvers_](https://docs.ens.domains/contract-api-reference/publicresolver) deben tenerse en cuenta las siguientes _interfaces_:

* [â€‹EIP 137](https://eips.ethereum.org/EIPS/eip-137) Direcciones (`addr()`).
* [EIP 165](https://eips.ethereum.org/EIPS/eip-165) DetecciÃ³n de interface (`supportsInterface()`).
* [EIP 181](https://eips.ethereum.org/EIPS/eip-181) ResoluciÃ³n reversa (`name()`).
* [EIP 634](https://eips.ethereum.org/EIPS/eip-634) Registros de texto (`text()`).

Se usarÃ¡ como dominio de primer nivel el nombre `iua`. Los dominios a utilizar serÃ¡n:

* `contratos.iua`: Dominio donde estÃ¡n los nombres de los contratos.
* `votantes.iua`: Dominio donde estÃ¡n los nombres de los votantes.

Los votantes deben poder registrar su cuenta asociÃ¡ndola con un nombre del dominio `votantes.iua`.

Los contratos creados deben registrarse con un nombre adecuado del dominio `contratos.iua`.

### MÃºltiples dueÃ±os y firma mÃºltiple (_multisig_)
El registro debe implementarse como un contrato con mÃºltiples dueÃ±os y firma mÃºltiple (ver [ejemplo](https://github.com/ethereum/dapp-bin/blob/master/wallet/wallet.sol)). Ciertas operaciones sÃ³lo pueden implementarse con la aprobaciÃ³n de una mayorÃ­a simple de los dueÃ±os. Por ejemplo:

* Agregar un dueÃ±o.
* Quitar un dueÃ±o.

## Contratos de votaciÃ³n
Al igual que en el Trabajo PrÃ¡ctico 6, debe existir un _contrato factorÃ­a_ que permite crear votaciones y acceder a ellas, tanto para administrarlas como para votar y consultar resultados. Los mÃ©todos provistos tanto por el contrato factorÃ­a como por las votaciones no deben ser necesariamente los mismos que en el prÃ¡ctico mencionado. Pueden eliminarse, agregarse o reemplazarse mÃ©todos segÃºn se considere adecuado.

El contrato de votaciÃ³n difiere del utilizado en el prÃ¡ctico:

* El registro y las autorizaciones para votar no se realizan en el contrato, sino en el ENS. Un votante manifiesta su interÃ©s en votar registrÃ¡ndose en el dominio `votantes.iua`. El dueÃ±o de una votaciÃ³n puede permitir o restringir el voto mediante el uso de registros (`records`) de tipo `text`. El contrato debe verificar si un votante estÃ¡ registrado y autorizado mediante consultas al resolvers correspondiente.
* El comienzo e inicio de la votaciÃ³n no se especifican mediante invocaciones explÃ­citas de un mÃ©todo, sino mediante _timestamps_ provistos en el constructor. Las votaciones comienzan y terminan automÃ¡ticamente en los horarios especificados.

## Interface web

### Usuario con Metamask
Debe poder ver las votaciones existentes
    
- Propuesta
- Fecha y hora de inicio
- Fecha y hora de finalizaciÃ³n
- Resultados

AdemÃ¡s debe poder:

- Registrarse como votante.
- Votar en las votaciones en las que estÃ¡ habilitado.
- Crear votaciones, indicando propuesta, nombre de dominio, fecha y hora de comienzo y fecha y hora de finalizaciÃ³n.
- Habilitar/deshabilitar votantes en las votaciones que Ã©l ha creado. 
