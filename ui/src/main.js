import "@babel/polyfill";
import Vue from "vue";
import "./plugins/vuetify";
import App from "./App.vue";
import VueMask from 'v-mask';

let factoryData = require("../../contract/build/contracts/BallotFactory.json");
let ballotData = require("../../contract/build/contracts/Ballot.json");

Vue.config.productionTip = false;
Vue.use(VueMask);

new Vue({
  render: h =>
    h(App, {
      props: {
        ballotData,
        factoryData
      }
    })
}).$mount("#app");
